import java.util.ArrayList;
import java.util.Random;

public class Mutation implements Runnable {

    private ArrayList<Individual> newIndividuals;
    private AlgorithmSelector algorithmSelector;
    private int individualSize;
    private int populationSize;
    private int counter;

    public Mutation(ArrayList<Individual> newIndividuals, AlgorithmSelector algorithmSelector, int individualSize, int populationSize) {
        this.newIndividuals = newIndividuals;
        this.algorithmSelector = algorithmSelector;
        this.individualSize = individualSize;
        this.populationSize = populationSize;
    }

    public int getCounter() {
        return counter;
    }

    @Override
    public void run() {
        synchronized (newIndividuals) {
            while (true) {
                if (counter < newIndividuals.size()) {
                    mutationProcess();
                    newIndividuals.notifyAll();
                } else if (counter == newIndividuals.size()) {
                    try {
                        newIndividuals.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else if (counter == populationSize) {
                    newIndividuals.notifyAll();
                    break;
                }
            }
        }
    }

    // Mutation Process, Mutate one Alleles by modifying the values between 0.5 and -0.5 from one Individual only
    // Iterate through every Individuals with 1% Mutation,
    // and if that individuals is selected for Mutation, one of its alleles will be change and move on.
    public void mutationProcess() {
        double maxIndividual = algorithmSelector.getMax();
        double minIndividual = algorithmSelector.getMin();
        double max = algorithmSelector.getMax() * 0.09765625;
        double min = algorithmSelector.getMin() * 0.09765625;
        Random random = new Random();

        for (int j = 0; j < individualSize; j++) {
            if (random.nextDouble() <= 0.01) { // 1% Chance to Mutate
                double temp = newIndividuals.get(counter).getChromosome()[j];
                temp += (random.nextDouble() * (max - min)) + min;

                // Check whether the alleles will exceed the boundary
                while (temp > maxIndividual || temp < minIndividual) {
                    temp = newIndividuals.get(counter).getChromosome()[j];
                    temp += (random.nextDouble() * (max - min)) + min;
                }

                newIndividuals.get(counter).getChromosome()[j] = temp;
            }
        }

        counter++;
    }
}