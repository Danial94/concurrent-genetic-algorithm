import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

public class GeneticAlgorithm extends Application {
    Generation generation = new Generation();

    public void start(Stage stage) throws InterruptedException {
        ExecutorService exec = Executors.newFixedThreadPool(5);
        generation.runGenerations(exec);
        stage.setTitle("Genetic Algorithm Results");

        // Defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Number of Generation");
        yAxis.setLabel("Fitness");
        String algorithmName = "";

        //setting the algorithm name for the graph
        if(generation.chooseAlgorithm==1) {
            algorithmName="Schwefel";
        }
        else if(generation.chooseAlgorithm==2) {
            algorithmName="Rosenbrock";
        }
        else if(generation.chooseAlgorithm==3) {
            algorithmName="Rastrigin";
        }
        else if(generation.chooseAlgorithm==4) {
            algorithmName="Simple";
        }
        
        // Creating the chart
        final LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setTitle("Minimum Fitness for "+algorithmName+" Function");

        // Defining a series
        XYChart.Series series = new XYChart.Series();
        series.setName("Genetic Algorithm");

        // Populating the series with data
        for (int i = 0 ; i < generation.getFitnessArray().length; i++) {
            series.getData().add(new XYChart.Data(i, generation.getFitnessArray()[i]));
        }

        Scene scene  = new Scene(lineChart, 700, 700);
        lineChart.getData().add(series);
        lineChart.setCreateSymbols(false);

        stage.setScene(scene);
        stage.show();

        exec.shutdown();
    }

    public static void main(String[] args) {
        launch();
    }
}