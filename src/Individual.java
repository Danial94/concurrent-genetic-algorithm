public class Individual {

    private double[] chromosome;
    private int selector;

    public Individual(double[] chromosome, int selector) {
        this.chromosome = chromosome;
        this.selector = selector;
    }

    // Evaluate the Individual with the Equation
    public double individualEvaluation() {
        AlgorithmSelector algorithmSelector = new AlgorithmSelector(selector);
        double sum = 0.0;

        switch(selector) {
            case 1: sum = algorithmSelector.schwefelFunction(chromosome);
                    break;
            case 2: sum = algorithmSelector.rosenbrockFunction(chromosome);
                    break;
            case 3: sum = algorithmSelector.rastriginFunction(chromosome);
                    break;
            case 4: sum = algorithmSelector.simpleFunction(chromosome);
                    break;        
        }

        return sum;
    }

    public double[] getChromosome() {
        return chromosome;
    }
}